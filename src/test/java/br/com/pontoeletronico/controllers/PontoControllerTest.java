package br.com.pontoeletronico.controllers;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.models.dtos.RespostaDTOPonto;
import br.com.pontoeletronico.services.PontoService;
import br.com.pontoeletronico.services.RegistroPonto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(PontoController.class)
public class PontoControllerTest {

    @MockBean
    private PontoService pontoService;

    @Autowired
    private MockMvc mockMvc;

    private Ponto ponto;
    private Usuario usuario;
    private RespostaDTOPonto respostaDTOPonto;
    private RegistroPonto registroPonto;
    private List<RegistroPonto> registroPontoList;

    @BeforeEach
    public void setUp(){
        ponto = new Ponto();
        usuario = new Usuario();
        respostaDTOPonto = new RespostaDTOPonto();
        registroPontoList = new ArrayList<>();

        usuario.setId((long)1);
        usuario.setNomeCompleto("Douglas Maldonado");
        usuario.setCpf("89439639202");
        usuario.setEmail("douglas.maldonado@gmail.com");

        ponto.setUsuario(usuario);
        ponto.setTipoBatida(TipoBatidaEnum.ENTRADA);

    }

    @Test
    public void testarMarcarPonto() throws Exception{
        Mockito.when(pontoService.baterPonto(Mockito.any(Ponto.class))).then(pontoObjeto -> {
            ponto.setId((long)1);
            ponto.setDataHoraBatida(LocalDateTime.now());

            return ponto;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonPonto = mapper.writeValueAsString(ponto);

        mockMvc.perform(MockMvcRequestBuilders.post("/marcacoes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonPonto))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarListarMarcacoes() throws Exception{
        registroPonto = new RegistroPonto();
        registroPonto.setTipoBatida(TipoBatidaEnum.ENTRADA);

        ponto.setId((long)1);

        registroPontoList.add(registroPonto);
        respostaDTOPonto.setMarcacoes(registroPontoList);
        respostaDTOPonto.setNomeUsuario(usuario.getNomeCompleto());
        respostaDTOPonto.setHorasTrabalhadas("08:00:00");

        Mockito.when(pontoService.listarPontoPorUsuario(Mockito.anyLong())).thenReturn(respostaDTOPonto);

        ObjectMapper mapper = new ObjectMapper();
        String jsonPonto = mapper.writeValueAsString(ponto);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/marcacoes/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonPonto))
                .andExpect(MockMvcResultMatchers.
                        jsonPath("$.nomeUsuario", CoreMatchers.equalTo("Douglas Maldonado")));

        MvcResult mvcResult = resultActions.andReturn();
        String jsonResposta = mvcResult.getResponse().getContentAsString();
        respostaDTOPonto = mapper.readValue(jsonResposta, RespostaDTOPonto.class);

    }

}
