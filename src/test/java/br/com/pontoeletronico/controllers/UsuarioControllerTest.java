package br.com.pontoeletronico.controllers;

import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    private Usuario usuario;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();

        usuario.setNomeCompleto("Douglas Maldonado");
        usuario.setCpf("89439639202");
        usuario.setEmail("douglas.maldonado@gmail.com");
    }

    @Test
    public void testarCadastrarUsuario() throws Exception{
        Mockito.when(usuarioService.salvarUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto -> {
            usuario.setId((long) 1);
            usuario.setDataDeCadastro(LocalDate.now());
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarAtualizarUsuario() throws Exception{
        Mockito.when(usuarioService.atualizarUsuario(Mockito.anyLong(), Mockito.any(Usuario.class)))
                .then(usuarioObjeto -> {
                    usuario.setId((long)1);
                    return usuario;
                });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));


    }

    @Test
    public void testarAtualizarUsuarioErro() throws Exception{
        Mockito.when(usuarioService.atualizarUsuario(Mockito.anyLong(), Mockito.any(Usuario.class)))
                .thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void testarConsultarPorId() throws  Exception{
        usuario.setId((long) 1);
        usuario.setDataDeCadastro(LocalDate.now());

        Mockito.when(usuarioService.consultarUsuario(Mockito.anyLong())).thenReturn(usuario);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarConsultarPorIdErro() throws  Exception{
        Mockito.when(usuarioService.consultarUsuario(Mockito.anyLong())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarConsultarTodos() throws  Exception{
        Mockito.when(usuarioService.consultarTodos()).then(usuarios -> {
            usuario.setId((long) 1);
            usuario.setDataDeCadastro(LocalDate.now());

            List<Usuario> usuarioList = new ArrayList<>();
            usuarioList.add(usuario);

            Iterable<Usuario> usuarioIterable = usuarioList;
            return usuarioIterable;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));

    }



}
