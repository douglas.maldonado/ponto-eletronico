package br.com.pontoeletronico.services;

import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    private Usuario usuario;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();

        usuario.setId((long)1);
        usuario.setDataDeCadastro(LocalDate.now());
        usuario.setNomeCompleto("Douglas Maldonado");
        usuario.setCpf("89439639202");
        usuario.setEmail("douglas.maldonado@gmail.com");

    }

    @Test
    public void testarSalvarUsuario(){
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);

        Assertions.assertEquals(LocalDate.now(), usuarioObjeto.getDataDeCadastro());
    }

    @Test
    public void testarAtualizarUsuario(){
        Optional<Usuario> usuarioOptional = Optional.of(usuario);

        Mockito.when(usuarioRepository.existsById(Mockito.anyLong())).thenReturn(true);
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(usuarioOptional);
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);


        Usuario usuarioObjeto = usuarioService.atualizarUsuario(usuario.getId(), usuario);

        Assertions.assertSame(usuarioObjeto, usuario);
    }

    @Test
    public void testarConsultarUsuario(){
        Optional<Usuario> usuarioOptional = Optional.of(usuario);

        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(usuarioOptional);

        Usuario usuarioObjeto = usuarioService.consultarUsuario(usuario.getId());

        Assertions.assertSame(usuarioObjeto, usuario);

    }

    @Test
    public void testarConsultarTodos(){
        Iterable<Usuario> usuarioIterable = Arrays.asList(usuario);

        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioIterable);

        Iterable<Usuario> usuarios = usuarioService.consultarTodos();

        Assertions.assertEquals(usuarioIterable, usuarios);

    }



}
