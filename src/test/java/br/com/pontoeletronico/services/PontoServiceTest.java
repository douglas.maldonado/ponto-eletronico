package br.com.pontoeletronico.services;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.models.dtos.RespostaDTOPonto;
import br.com.pontoeletronico.repositories.PontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class PontoServiceTest {

    @MockBean
    private PontoRepository pontoRepository;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private PontoService pontoService;

    private Ponto ponto;
    private Usuario usuario;
    private RespostaDTOPonto respostaDTOPonto;
    private RegistroPonto registroPonto;
    private List<RegistroPonto> registroPontoList;
    private List<Ponto> pontos;

    @BeforeEach
    public void setUp(){
        pontos = new ArrayList<>();

        ponto = new Ponto();
        ponto.setId((long)1);
        ponto.setDataHoraBatida(LocalDateTime.now());
        ponto.setTipoBatida(TipoBatidaEnum.ENTRADA);

        usuario = new Usuario();
        usuario.setId((long)1);
        usuario.setNomeCompleto("Douglas Maldonado");
        usuario.setCpf("89439639202");
        usuario.setEmail("douglas.maldonado@gmail.com");
        ponto.setUsuario(usuario);

        pontos.add(ponto);

        registroPonto = new RegistroPonto();
        respostaDTOPonto = new RespostaDTOPonto();
        registroPontoList = new ArrayList<>();

    }

    @Test
    public void testarBaterPonto(){
        Mockito.when(usuarioService.consultarUsuario(Mockito.anyLong())).thenReturn(usuario);
        Mockito.when(pontoRepository.save(Mockito.any(Ponto.class))).thenReturn(ponto);

        Ponto pontoObjeto = pontoService.baterPonto(ponto);

        Assertions.assertEquals(pontoObjeto, ponto);
    }


}
