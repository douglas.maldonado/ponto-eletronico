package br.com.pontoeletronico.services;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.models.dtos.RespostaDTOPonto;
import br.com.pontoeletronico.repositories.PontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class PontoService {

    @Autowired
    private PontoRepository pontoRepository;

    @Autowired
    private UsuarioService usuarioService;

    public Ponto baterPonto(Ponto ponto){
        LocalDateTime dataHora = LocalDateTime.now();

        Usuario usuario = usuarioService.consultarUsuario(ponto.getUsuario().getId());
        ponto.setUsuario(usuario);
        ponto.setDataHoraBatida(dataHora);

        Ponto pontoObjeto = pontoRepository.save(ponto);
        return pontoObjeto;
    }

    public RespostaDTOPonto listarPontoPorUsuario(Long id){
        RespostaDTOPonto respostaDTOPonto = new RespostaDTOPonto();
        List<RegistroPonto> registroPontoList = new ArrayList<>();
        List<Ponto> pontoList = new ArrayList<>();

        Usuario usuario = usuarioService.consultarUsuario(id);
        Iterable<Ponto> pontoObjeto = pontoRepository.findAllByUsuario(usuario);

        pontoObjeto.forEach(pontoList::add);
        for(Ponto ponto: pontoList){
            RegistroPonto registroPonto = new RegistroPonto();
            registroPonto.setDataHoraBatida(ponto.getDataHoraBatida());
            registroPonto.setTipoBatida(ponto.getTipoBatida());

            registroPontoList.add(registroPonto);
        }

        Long totalHoras = calcularQtdeHoras(pontoList);

        String horasTrabalhadas = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(totalHoras),
                TimeUnit.MILLISECONDS.toMinutes(totalHoras) -
                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalHoras)),
                TimeUnit.MILLISECONDS.toSeconds(totalHoras) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalHoras)));

        respostaDTOPonto.setNomeUsuario(usuario.getNomeCompleto());
        respostaDTOPonto.setMarcacoes(registroPontoList);
        respostaDTOPonto.setHorasTrabalhadas(horasTrabalhadas);

        return respostaDTOPonto;

    }

    public Long calcularQtdeHoras(List<Ponto> pontos){
        LocalDateTime dataHoraAtual;
        LocalDateTime dataHoraAnterior = null;
        Long totalHoras = (long) 0;

        Collections.sort(pontos, Comparator.comparing(Ponto::getDataHoraBatida));

        for(Ponto ponto: pontos){
            if (ponto.getTipoBatida().equals(TipoBatidaEnum.ENTRADA)){
                dataHoraAnterior = ponto.getDataHoraBatida();
            }
            if(ponto.getTipoBatida().equals((TipoBatidaEnum.SAIDA))){
                dataHoraAtual = ponto.getDataHoraBatida();

                if (dataHoraAnterior == null){
                    throw new RuntimeException("Marcacao Impar, Solicitar ajuste de Marcacao");
                }

                Long periodoHoras = Duration.between(dataHoraAnterior, dataHoraAtual).toMillis();

                totalHoras = totalHoras + periodoHoras;

                dataHoraAnterior = null;
            }

        }

        return totalHoras;

    }





}
