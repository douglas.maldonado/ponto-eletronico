package br.com.pontoeletronico.services;

import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario){
        LocalDate data = LocalDate.now();

        if(usuario.getDataDeCadastro() == null){
            usuario.setDataDeCadastro(data);
        }

        Usuario usuarioObjeto = usuarioRepository.save(usuario);

        return usuarioObjeto;
    }

    public Usuario atualizarUsuario(Long id, Usuario usuario){

        if (usuarioRepository.existsById(id)){
            usuario.setId(id);

            Usuario usuario1 = consultarUsuario(id);
            usuario.setDataDeCadastro(usuario1.getDataDeCadastro());

            Usuario usuarioObjeto = salvarUsuario(usuario);
            return usuarioObjeto;
        }

        throw new RuntimeException("Usuario nao encontrado");

    }

    public Usuario consultarUsuario(Long id){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);

        if(optionalUsuario.isPresent()){
            return optionalUsuario.get();
        }

        throw new RuntimeException("Usuario não cadastrado no sistema");
    }

    public Iterable<Usuario> consultarTodos(){
        Iterable<Usuario> usuarios = usuarioRepository.findAll();

        return usuarios;
    }

}
