package br.com.pontoeletronico.services;

import br.com.pontoeletronico.enums.TipoBatidaEnum;

import java.time.LocalDateTime;

public class RegistroPonto {
    private LocalDateTime dataHoraBatida;
    private TipoBatidaEnum tipoBatida;

    public RegistroPonto() {
    }

    public LocalDateTime getDataHoraBatida() {
        return dataHoraBatida;
    }

    public void setDataHoraBatida(LocalDateTime dataHoraBatida) {
        this.dataHoraBatida = dataHoraBatida;
    }

    public TipoBatidaEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatidaEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
