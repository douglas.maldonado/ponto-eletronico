package br.com.pontoeletronico.models;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@JsonIgnoreProperties(value = {"dataHoraBatida"}, allowGetters = true)
public class Ponto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @NotNull(message = "Usuario não pode ser nulo")
    private Usuario usuario;

    private LocalDateTime dataHoraBatida;

    @NotNull(message = "O campo Tipo da Batida não pode ser nulo")
    private TipoBatidaEnum tipoBatida;

    public Ponto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getDataHoraBatida() {
        return dataHoraBatida;
    }

    public void setDataHoraBatida(LocalDateTime dataHoraBatida) {
        this.dataHoraBatida = dataHoraBatida;
    }

    public TipoBatidaEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatidaEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
