package br.com.pontoeletronico.models.dtos;

import br.com.pontoeletronico.services.RegistroPonto;

import java.util.ArrayList;
import java.util.List;

public class RespostaDTOPonto {
    private String nomeUsuario;
    private List<RegistroPonto> marcacoes = new ArrayList<>();
    private String horasTrabalhadas;

    public RespostaDTOPonto() {
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public List<RegistroPonto> getMarcacoes() {
        return marcacoes;
    }

    public void setMarcacoes(List<RegistroPonto> marcacoes) {
        this.marcacoes = marcacoes;
    }

    public String getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public void setHorasTrabalhadas(String horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }
}
