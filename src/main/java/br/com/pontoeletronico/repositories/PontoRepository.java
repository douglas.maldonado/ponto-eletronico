package br.com.pontoeletronico.repositories;

import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface PontoRepository extends CrudRepository<Ponto, Long> {
    Iterable<Ponto> findAllByUsuario(Usuario usuario);
}
