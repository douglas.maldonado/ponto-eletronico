package br.com.pontoeletronico.controllers;

import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.dtos.RespostaDTOPonto;
import br.com.pontoeletronico.services.PontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/marcacoes")
public class PontoController {

    @Autowired
    private PontoService pontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Ponto marcarPonto(@RequestBody @Valid Ponto ponto){

        try{
            Ponto pontoObjeto = pontoService.baterPonto(ponto);
            return pontoObjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }

    @GetMapping("/{id}")
    public RespostaDTOPonto listarMarcacoes(@PathVariable(name = "id") Long id){

        try{
            RespostaDTOPonto respostaDTOPonto = pontoService.listarPontoPorUsuario(id);
            return respostaDTOPonto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }

}
