package br.com.pontoeletronico.controllers;

import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario cadastrarUsuario(@RequestBody @Valid Usuario usuario){
        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);
        return usuarioObjeto;
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario atualizarUsuario(@PathVariable(name = "id") Long id, @RequestBody Usuario usuario){
        try{
            Usuario usuarioObjeto = usuarioService.atualizarUsuario(id, usuario);
            return usuarioObjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }

    @GetMapping("/{id}")
    public Usuario consultarPorId (@PathVariable(name = "id") Long id){
        try{
            Usuario usuario = usuarioService.consultarUsuario(id);
            return usuario;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public Iterable<Usuario> consultarTodos(){
        Iterable<Usuario> usuarios = usuarioService.consultarTodos();
        return usuarios;
    }

}
