package br.com.pontoeletronico.exceptions;

import br.com.pontoeletronico.exceptions.erros.MensagemDeErro;
import br.com.pontoeletronico.exceptions.erros.ObjetoDeErro;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public MensagemDeErro manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception){
        HashMap<String, ObjetoDeErro> erros = new HashMap<>();

        BindingResult result = exception.getBindingResult();

        for (FieldError error: result.getFieldErrors()){
            erros.put(error.getField(),
                    new ObjetoDeErro(error.getDefaultMessage(), error.getRejectedValue().toString()));
        }

        MensagemDeErro mensagemDeErro = new MensagemDeErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
                "Erro de validação dos campos enviados", erros);

        return mensagemDeErro;

    }
}
