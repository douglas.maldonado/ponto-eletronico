## Usuarios

### POST /usuarios
Cadastra Usuarios no Sistema.

**Request Body**
```json
{
    "nomeCompleto": "Colaborador 3",
    "cpf": "73793237257",
    "email": "col3@gmail.com"
}
```

**Response 201**
```json
{
    "id": 3,
    "nomeCompleto": "Colaborador 3",
    "cpf": "73793237257",
    "email": "col3@gmail.com",
    "dataDeCadastro": "2020-07-05"
}
```

### GET /usuarios
Exibe a Lista dos Usuarios Cadastrados no Sistema.

**Response 200**
```json
[
    {
        "id": 1,
        "nomeCompleto": "Colaborador 1",
        "cpf": "89439639202",
        "email": "col1@gmail.com",
        "dataDeCadastro": "2020-07-05"
    },
    {
        "id": 2,
        "nomeCompleto": "Colaborador 2",
        "cpf": "47388481516",
        "email": "col2@gmail.com",
        "dataDeCadastro": "2020-07-05"
    },
    {
        "id": 3,
        "nomeCompleto": "Colaborador 3",
        "cpf": "73793237257",
        "email": "col3@gmail.com",
        "dataDeCadastro": "2020-07-05"
    }
]
```

### GET /usuarios/{id}
Exibe Usuarios com o id solicitado.

**Response 200**
```json
{
    "id": 1,
    "nomeCompleto": "Colaborador 1",
    "cpf": "89439639202",
    "email": "col1@gmail.com",
    "dataDeCadastro": "2020-07-05"
}
```

### PUT /usuarios
Atualiza Dados do Usuario no Sistema.

**Request Body**
```json
{
    "nomeCompleto": "Colaborador 1",
    "cpf": "89439639202",
    "email": "colab1@gmail.com"
}
```

**Response 201**
```json
{
    "id": 1,
    "nomeCompleto": "Colaborador 1",
    "cpf": "89439639202",
    "email": "colab1@gmail.com",
    "dataDeCadastro": "2020-07-05"
}
```

## Ponto

### POST /marcacoes
Faz a marcação de Ponto no Sistema.

**Request Body**
```json
{
    "usuario": {"id": 1},
    "tipoBatida": "ENTRADA"
}
```

**Response 201**
```json
{
    "id": 1,
    "usuario": {
        "id": 1,
        "nomeCompleto": "Colaborador 1",
        "cpf": "89439639202",
        "email": "colab1@gmail.com",
        "dataDeCadastro": "2020-07-05"
    },
    "dataHoraBatida": "2020-07-05T23:04:47.34",
    "tipoBatida": "ENTRADA"
}
```

### POST /marcacoes/{id} 
Lista todas as marcações do id do Usuario selecionado.

**Response 200**
```json
{
    "nomeUsuario": "Colaborador 1",
    "marcacoes": [
        {
            "dataHoraBatida": "2020-07-05T23:04:29.301",
            "tipoBatida": "ENTRADA"
        },
        {
            "dataHoraBatida": "2020-07-05T23:04:37.021",
            "tipoBatida": "SAIDA"
        },
        {
            "dataHoraBatida": "2020-07-05T23:04:42.876",
            "tipoBatida": "ENTRADA"
        },
        {
            "dataHoraBatida": "2020-07-05T23:04:47.34",
            "tipoBatida": "SAIDA"
        }
    ],
    "horasTrabalhadas": "00:00:12"
}
```
